﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderBehav : MonoBehaviour
{
    public TMPro.TextMeshProUGUI Value_Text;
    public Slider slider;

    private void Start()
    {
        slider = GetComponent<Slider>();
    }

    void Update()
    {
        Value_Text.text = "" + slider.value;
    }
}
