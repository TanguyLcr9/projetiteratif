﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SoundManager : MonoBehaviour
{
    SoundManager Instance;
    public Light[] lightComp;

    public FMOD.Studio.EventInstance Sono;

    public Slider[] Slider;
    public Color[] col;

    enum ColorGradient
    {
        Green,
    }

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }

        Sono = FMODUnity.RuntimeManager.CreateInstance("event:/New Event");
        Sono.start();
       
        
    }

    private void Start()
    {
        //Sono =
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Sono, GetComponent<Transform>(), GetComponent<Rigidbody>());
        StartCoroutine(FadeColor());
    }

    private void Update()
    {
        for (int i = 0; i < lightComp.Length; i++)
        {
            lightComp[i].color = col[i];
        }
    }
    public IEnumerator FadeColor()
    {
        while (true)
        {
            float time = Random.Range(1f, 3f);
            for (int i = 0; i < lightComp.Length; i++) {
                lightComp[i].DOIntensity(Random.Range(0.4f, 1), time);
            }

            yield return new WaitForSeconds(time);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Sono.setParameterByName("Volume", Slider[0].value);
        Sono.setParameterByName("Param2", Slider[1].value);
        Sono.setParameterByName("Param3", Slider[2].value);
    }
}
